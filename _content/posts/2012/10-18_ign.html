--- 
title: Institut Géographique National 
layout: post 
category: casestudy 
tags: [geoconcept, france] 
author: Paul Ramsey 
thumbnail: ign.jpg
---

The [Institut Géographique National](http://ign.fr/) (IGN) is the national mapping agency for
France, with 1800 employees and a mandate to collect, integrate, manage and
distribute reference geographical information for the whole country.

<!-- pagebreak -->

<img class='right' src='{{ asset.ign }}' />
For many years, IGN has managed the national spatial data using the [GeoConcept](http://www.geoconcept.com/)
desktop GIS system. As with other desktop GIS systems, using GeoConcept required
that the data be broken up into map tiles for management.

IGN began exploring storing their data in a seamless database for the entire
country around 2002. They benchmarked several technologies, including Oracle,
DB2 and PostGIS/PostgreSQL, looking for a product that met a number of
requirements:

* The capacity to handle more than 100 million spatial objects; 
* The speed to answer queries quickly given such a large database; and, 
* Full transactional integrity to ensure data quality during maintenance 
  operations.

The benchmarking tests showed PostgreSQL/PostGIS with similar performance to the
proprietary alternatives.

<img class='left' src='{{ asset.other1BDUniScale1000_sm }}' />
Frank Fuchs is a project manager at IGN, and was part of the team that selected
PostGIS. "Since PostgreSQL and PostGIS are free and open source software, we
could use them quietly in a prototype. On the other hand, if we used a
commercial DBMS, we could later have a problem in a call for tenders", said
Fuchs. "And also, the low price [of PostGIS] also was a good point."

Fuchs' project team has integrated PostGIS into the data management system for
the French "BDUni" database — a 3D national integrated database containing
roads, rails, hydrography, vegetation, buildings, administrative boundaries and
more. They defined a new process and new tools for managing all the BDUni data
in a seamless national database, fitting the new data store into the existing
tools familiar to the IGN field staff. The team settled on PostGIS as the
backend database, and retained GeoConcept as the editing environment for
updating data. The GeoConcept software was modified to add new tools for
checking data into and out of the central database:

* A data loader that takes data from the existing GeoConcept flat file archive
  and loads it into the PostGIS database. This is used once only, when a file is
  moved into the new management system.

* A data extractor that pulls a specific working unit of data out of the
  database and into GeoConcept for editing. Working units are allowed to overlap,
  and several updaters can extract the same feature simultaneously. Overlapping
  the working units removes the old filed-based problem of edge matching.

* A data synchronizer that pushes updates from the client to the database, and
  pulls changes in the database down to the client. If two people have updated the
  same feature, then a conflict is raised, and the operator has to choose how to
  resolve it.

By using the full GeoConcept GIS as an editing environment, the new system
allows researchers to work fully disconnected in the field, updating the
database as necessary when they have connectivity to the main IGN network. Field
researchers do not have to learn a new GIS system for editing the data, as they
are already familiar with GeoConcept. The extraction and synchronization tools
make heavy use of PostgreSQL's transactional system, to ensure that the data
integrity is not violated in cases of unexpected network failure or
disconnection in the middle of a synchronization.

As of October, 2006, the BDUni system is being used by 40 field researchers in 6
different cities in France. Just over 1/4 of France has been loaded into the
database so far, about 30 million features. The first features were loaded in
July of 2006. By the end of 2006, 130 field researchers are expected to be using
the system. In keeping with the requirement for high reliability and data
integrity, the system also includes a redundant backup server, which is
synchronized with the main server several times a day.

Having completed this project, Fuchs has this advice for other managers,
"Remember that databases are very powerful tools, and transactions are a key
feature. PostGIS brings these tools to the GIS community very efficiently."
