This site can be viewed and generated to HTML using 
PieCrust <http://bolt80.com/piecrust/>

- Download PieCrust
- Add the ./bin dir to your PATH

Run 

  chef serve .

to get a test server on top of this code that you
can access at http://localhost:8080

For production we will use 'chef bake' to snap
out a static copy.
